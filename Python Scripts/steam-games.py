# Algorithm By Steam-games Enterprise! All Rights Reserved! Open Source 4ever. Like and Follow Project
# in GitLab - https://gitlab.com/BDAg/steam-games

#Libraries
from bs4 import BeautifulSoup
import pandas as pd
import requests
import pymongo
import csv
import re
import sys
import os
import datetime
import selenium
import json
import time
import schedule
import robobrowser
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class EmailBot():
    # Configuração
    host = '<HOST_URL>'
    port = 587
    user = '<USUARIO>'
    password = '<SENHA>'

    # Criando objeto
    print('Criando objeto servidor...')
    server = smtplib.SMTP(host, port)

    # Login com servidor
    print('Login...')
    server.ehlo()
    server.starttls()
    server.login(user, password)

    # Criando mensagem
    message = 'Olá, mundo!'
    print('Criando mensagem...')
    email_msg = MIMEMultipart()
    email_msg['From'] = user
    email_msg['To'] = '<EMAIL_DE_DESTINO>'
    email_msg['Subject'] = 'Assunto da mensagem'
    print('Adicionando texto...')
    email_msg.attach(MIMEText(message, 'plain'))

    print('Obtendo arquivo...')
    filename = 'nome' 
    filepath = 'pasta/nome'
    attachment = open(filepath, 'rb')

    print('Lendo arquivo...')
    att = MIMEBase('application', 'octet-stream')
    att.set_payload(attachment.read())
    encoders.encode_base64(att)
    att.add_header('Content-Disposition', f'attachment; filename= {filename}')

    attachment.close()
    print('Adicionando arquivo ao email...')
    email_msg.attach(att)

    # Enviando mensagem
    print('Enviando mensagem...')
    server.sendmail(msg['From'], msg['To'], msg.as_string())
    print('Mensagem enviada!')
    server.quit()

class WhatsAppBot():
    def __init__(self, json):
        self.json = json
        self.dict_messages = json['messages']
        self.APIUrl = 'https://eu41.chat-api.com/instance12345/'
        self.token = 'abcdefg'

    def send_requests(self, method, data):
        url = f"{self.APIUrl}{method}?token={self.token}"
        headers = {'Content-type': 'application/json'}
        answer = requests.post(url, data=json.dumps(data), headers=headers)
        return answer.json()

    def send_message(self, chatId, text):
        data = {"chatId" : chatId,
                "body" : text}
        answer = self.send_requests('sendMessage', data)
        return answer

    def welcome(self,chatId, noWelcome = False):
        welcome_string = ''
        if (noWelcome == False):
            welcome_string = "WhatsApp Demo Bot Python\n"
        else:
            welcome_string = """Incorrect command
        Commands:
        1. chatId - show ID of the current chat
        2. time - show server time
        3. me - show your nickname
        4. file [format] - get a file. Available formats: doc/gif/jpg/png/pdf/mp3/mp4
        5. ptt - get a voice message
        6. geo - get a location
        7. group - create a group with the bot"""
        return self.send_message(chatId, welcome_string)

        def show_chat_id(self,chatId):
            return self.send_message(chatId, f"Chat ID : {chatId}")

        def time(self, chatId):
            t = datetime.datetime.now()
            time = t.strftime('%d:%m:%Y')
            return self.send_message(chatId, time)

        def me(self, chatId, name):
            return self.send_message(chatId, name)

class TelegramBot():
    def telegram_bot_sendtext(bot_message):
        bot_token = ''
        bot_chatID = ''
        send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
        response = requests.get(send_text)
        return response.json()

    def report():
        my_balance = 10   ## Replace this number with an API call to fetch your account balance
        my_message = "Current balance is: {}".format(my_balance)   ## Customize your message
        telegram_bot_sendtext(my_message)
        schedule.every().day.at("12:00").do(report)

        while True:
            schedule.run_pending()
            time.sleep(1)

    #Libraries Versions
    #print("BeautifulSoup ->", beautifulsoup4.__version__)
    #print("Pandas ->", pandas.__version__)
    #print("Requests ->", requests.__version__)
    #print("Pymongo ->", pymongo.__version__)
    #print("Csv ->", csv.__version__)
    #print("Re ->", re.__version__)
    #print("Sys ->", sys.__version__)
    #print("Sys ->", sys.__version__)
    #print("Os ->", os.__version__)
    #print("Datetime ->", datetime.__version__)
    #print("Selenium ->", selenium.__version__)
    #print("Robobrowser ->", robobrowser.__version__)

#Contact Information
HEADERS = {
    'User-Agent': 'Marcelo Augusto Delgado Jordão, marcelo.jordao@fatec.sp.gov.br',
    'From': 'marcelo.jordao@fatec.sp.gov.br'
}

URL_CONTACT = 'https://www.gitlab.com/marcelinhoshow1'

PAGES_GET = requests.get(URL_CONTACT, headers = HEADERS)
if PAGES_GET.status_code == 200:
    print('')
    content = PAGES_GET.content
else:
    print('Server down or incorrect domain')

# HTTP Exceptions Handling
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError
from bs4 import BeautifulSoup
try:
    html = urlopen("https://store.steampowered.com/search/?sort_by=Released_DESC&maxprice=free")
except HTTPError as e:
    print(e)
except URLError:
    print("Server down or incorrect domain")
else:
    res = BeautifulSoup(html.read(),"html5lib")
    if res.title is None:
        print("Tag not found")
    else:
        print(res.title)

# URLS Exceptions Handling
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError
from bs4 import BeautifulSoup
try:
    html = urlopen("https://store.steampowered.com/search/?sort_by=Released_DESC&maxprice=free")
except HTTPError as e:
    print(e)
except URLError:
    print("Server down or incorrect domain")
else:
    res = BeautifulSoup(html.read(),"html5lib")
    if res.title is None:
        print("Tag not found")
    else:
        print(res.title)

# URL Exceptions Handling
try:
    html = urlopen("https://store.steampowered.com/search/?sort_by=Released_DESC&maxprice=free")
except HTTPError as e:
    print(e)
except URLError:
    print("Server down or incorrect domain")
else:    
    res = BeautifulSoup(html.read(),"html5lib")

    #GAMES VETOR
    IMAGEM = []
    NOME = []
    EXCLUSIVIDADE = []
    SISTEMA_OPERACIONAL = []
    TIPO_DE_JOGO = []
    DATA_DE_ATUALIZACAO = []
    VALOR_DO_JOGO = []
    AVALIACOES = []

    i = ''

    #SPECIAL_SECTIONS

    #SPECIAL_SECTIONS - FREE_TO_PLAY
    PAGE_FREE_TO_PLAY = 'https://store.steampowered.com/genre/Free%20to%20Play/#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS - DEMONSTRATIONS
    PAGE_DEMONSTRATIONS = 'https://store.steampowered.com/demos/#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS - EARLY_ACCESS
    PAGE_EARLY_ACESS = 'https://store.steampowered.com/genre/Early%20Access/#p=' + str(i) + '&tab=NewReleases'

    #REMOTE_PLAY

    #REMOTE_PLAY - COMPATIBLE_WITH_CONTROLS
    PAGE_COMPATIBLE_WITH_CONTROLS = 'https://store.steampowered.com/controller/#p=' + str(i) + '&tab=NewReleases'

    #REMOTE_PLAY - TOGETHER
    PAGE_REMOTEPLAY_TOGETHER = 'https://store.steampowered.com/remoteplay_together#p=s' + str(i) + '&tab=NewReleases'

    #REMOTE_PLAY - PHONE
    PAGE_REMOTEPLAY_PHONE = 'https://store.steampowered.com/remoteplay_phone/#p=' + str(i) + '&tab=NewReleases'

    #REMOTE_PLAY - TABLET
    PAGE_REMOTEPLAY_TABLET = 'https://store.steampowered.com/remoteplay_tablet/#p=' + str(i) + '&tab=NewReleases'

    #REMOTE_PLAY - REMOTEPLAY_TV
    PAGE_REMOTEPLAY_TV = 'https://store.steampowered.com/remoteplay_tv/#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS  - SOFTWARE
    PAGE_SOFTWARE = 'https://store.steampowered.com/software/#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS  - SOUNDTRACKS
    PAGE_SOUNDTRACKS = 'https://store.steampowered.com/soundtracks#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS  - VR
    PAGE_VR = 'https://store.steampowered.com/vr/#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS  - MAC_OS
    PAGE_MACOS = 'https://store.steampowered.com/macos#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS  - LINUX
    PAGE_LINUX = 'https://store.steampowered.com/linux#p=' + str(i) + '&tab=NewReleases'

    #SPECIAL_SECTIONS - CYBER_COFFES
    PAGE_CYBER_CAFES = 'https://store.steampowered.com/pccafe/#p=' + str(i) + '&tab=NewReleases'

    #GENRES

    #ACTION

    #ACTION - ARCADE_RHYTHM
    PAGE_ARCADE_RHYTHM = 'https://store.steampowered.com/category/arcade_rhythm/#p=' + str(i) + '&tab=NewReleases'

    #ACTION - FIGHTING_MARTIAL_ARTS
    PAGE_FIGHTING_MARTIAL_ARTS = 'https://store.steampowered.com/category/fighting_martial_arts/#p=' + str(i) + '&tab=NewReleases'

    #ACTION - ACTION_RUN_JUMP
    PAGE_ACTION_RUN_JUMP = 'https://store.steampowered.com/category/action_run_jump/#p=' + str(i) + '&tab=NewReleases'

    #ACTION - ACTION_BEAT_EM_UP
    PAGE_ACTION_BEAT_EM_UP = 'https://store.steampowered.com/category/action_beat_em_up/#p=' + str(i) + '&tab=NewReleases'

    #ACTION - ACTION_ROGUE_LIKE
    PAGE_ACTION_ROGUE_LIKE = 'https://store.steampowered.com/category/action_rogue_like/#p=' + str(i) + '&tab=NewReleases'

    #ACTION - ACTION_TPS
    PAGE_ACTION_TPS = 'https://store.steampowered.com/category/action_tps/#p=' + str(i) + '&tab=NewReleases'

    #ACTION - ACTION_FPS
    PAGE_ACTION_FPS = 'https://store.steampowered.com/category/action_fps/#p=' + str(i) + '&tab=NewReleases'

    #ADVENTURE_AND_CASUAL

    #ADVENTURE_AND_CASUAL - ADVENTURE
    PAGE_ADVENTURE = 'https://store.steampowered.com/category/adventure/#p=' + str(i) + '&tab=NewReleases'

    #ADVENTURE_AND_CASUAL - CASUAL
    PAGE_CASUAL = 'https://store.steampowered.com/category/casual/#p=' + str(i) + '&tab=NewReleases'

    #ADVENTURE_AND_CASUAL - METROIDVANIA
    PAGE_METROIDVANIA = 'https://store.steampowered.com/category/metroidvania/#p=' + str(i) + '&tab=NewReleases'

    #ADVENTURE_AND_CASUAL - PUZZLE_MATCHING
    PAGE_PUZZLE_MATCHING = 'https://store.steampowered.com/category/puzzle_matching/#p=' + str(i) + '&tab=NewReleases'

    #ADVENTURE_AND_CASUAL - ADVENTURE_RPG
    PAGE_ADVENTURE_RPG = 'https://store.steampowered.com/category/adventure_rpg/#p=' + str(i) + '&tab=NewReleases'

    #ADVENTURE_AND_CASUAL - VISUAL_NOVEL
    PAGE_VISUAL_NOVE = 'https://store.steampowered.com/category/visual_novel/#p=' + str(i) + '&tab=NewReleases'

    #ADVENTURE_AND_CASUAL - INTERACTIVE_FICTION
    PAGE_INTERACTIVE_FICTION = 'https://store.steampowered.com/category/interactive_fiction/#p=' + str(i) + '&tab=NewReleases'

    #RPG

    #RPG - RPG_JRPG
    PAGE_RPG_JRPG = 'https://store.steampowered.com/category/rpg_jrpg/#p=' + str(i) + '&tab=NewReleases'

    #RPG - RPG_ACTION
    PAGE_RPG_ACTION = 'https://store.steampowered.com/category/rpg_action/#p=' + str(i) + '&tab=NewReleases'

    #RPG - RPG_STRATEGY_TATICS
    PAGE_RPG_STRATEGY_TATICS = 'https://store.steampowered.com/category/rpg_strategy_tactics/#p=' + str(i) + '&tab=NewReleases'

    #RPG - ADVENTURE_RPG
    PAGE_ADVENTURE_RPG = 'https://store.steampowered.com/category/adventure_rpg/#p=' + str(i) + '&tab=NewReleases'

    #RPG - RPG_PARTY_BASED
    PAGE_RPG_PARTY_BASED = 'https://store.steampowered.com/category/rpg_party_based/#p=' + str(i) + '&tab=NewReleases'

    #RPG - RPG_TURN_BASED
    PAGE_RPG_TURN_BASED = 'https://store.steampowered.com/category/rpg_turn_based/#p=' + str(i) + '&tab=NewReleases'

    #RPG - ROGUE_LIKE_ROGUE_LITE
    PAGE_ROGUE_LIKE_ROGUE_LITE = 'https://store.steampowered.com/category/rogue_like_rogue_lite/#p=' + str(i) + '&tab=NewReleases'

    #SIMULATOR

    #SIMULATOR - SIM_BUILDING_AUTOMATION
    PAGE_SIM_BUILDING_AUTOMATION = 'https://store.steampowered.com/category/sim_building_automation/#p=' + str(i) + '&tab=NewReleases'

    #SIMULATOR - SIM_DATING
    PAGE_SIM_DATING = 'https://store.steampowered.com/category/sim_dating/#p=' + str(i) + '&tab=NewReleases'

    #SIMULATOR - SIM_SPACE_FLIGHT
    PAGE_SIM_SPACE_FLIGHT = 'https://store.steampowered.com/category/sim_space_flight/#p=' + str(i) + '&tab=NewReleases'

    #SIMULATOR - SIM_PHYSICS_SANDBOX
    PAGE_SIM_PHYSICS_SANDBOX = 'https://store.steampowered.com/category/sim_physics_sandbox/#p=' + str(i) + '&tab=NewReleases'

    #SIMULATOR - SIM_BUSINESS_TYCOON
    PAGE_SIM_BUSINESS_TYCOON = 'https://store.steampowered.com/category/sim_business_tycoon/#p=' + str(i) + '&tab=NewReleases'

    #SIMULATOR - SIM_FARMING_CRAFTING
    PAGE_SIM_FARMING_CRAFTING = 'https://store.steampowered.com/category/sim_farming_crafting/#p=' + str(i) + '&tab=NewReleases'

    #SIMULATOR - SIM_LIFE
    PAGE_SIM_LIFE = 'https://store.steampowered.com/category/sim_life/#p=' + str(i) + '&tab=NewReleases'

    #STRATEGY

    #STRATEGY - STRATEGY_CITIES_SETTLEMENTS
    PAGE_STRATEGY_CITIES_SETTLEMENTS = 'https://store.steampowered.com/category/strategy_cities_settlements/#p=' + str(i) + '&tab=NewReleases'

    #STRATEGY - TOWER_DEFENSE
    PAGE_TOWER_DEFENSE = 'https://store.steampowered.com/category/tower_defense/#p=' + str(i) + '&tab=NewReleases'

    #STRATEGY - STRATEGY_TURN_BASED
    PAGE_STRATEGY_TURN_BASED = 'https://store.steampowered.com/category/strategy_turn_based/#p=' + str(i) + '&tab=NewReleases'

    #STRATEGY - STRATEGY_REAL_TIME
    PAGE_STRATEGY_REAL_TIME = 'https://store.steampowered.com/category/strategy_real_time/#p=' + str(i) + '&tab=NewReleases'

    #STRATEGY - STRATEGY_GRAND_4X
    PAGE_STRATEGY_GRAND_4X = 'https://store.steampowered.com/category/strategy_grand_4x/#p=' + str(i) + '&tab=NewReleases'

    #STRATEGY - STRATEGY_MILITARY
    PAGE_STRATEGY_MILITARY = 'https://store.steampowered.com/category/strategy_military/#p=' + str(i) + '&tab=NewReleases'

    #STRATEGY - STRATEGY_CARD_BOARD
    PAGE_STRATEGY_CARD_BOARD = 'https://store.steampowered.com/category/strategy_card_board/#p=' + str(i) + '&tab=NewReleases'

    #SPORT_AND_RACING

    #SPORT_AND_RACING - RACING
    PAGE_RACING = 'https://store.steampowered.com/category/racing/#p=' + str(i) + '&tab=NewReleases'

    #SPORT_AND_RACING - SPORTS_TEAM
    PAGE_SPORTS_TEAM = 'https://store.steampowered.com/category/sports_team/#p=' + str(i) + '&tab=NewReleases'

    #SPORT_AND_RACING - SPORTS
    PAGE_SPORTS = 'https://store.steampowered.com/category/sports/#p=' + str(i) + '&tab=NewReleases'

    #SPORT_AND_RACING - SPORTS_INDIVIDUAL
    PAGE_SPORTS_INDIVIDUAL = 'https://store.steampowered.com/category/sports_individual/#p=' + str(i) + '&tab=NewReleases'

    #SPORTS_AND_RACING - SPORTS_FISHING_HUNTING
    PAGE_SPORTS_FISHING_HUNTING = 'https://store.steampowered.com/category/sports_fishing_hunting/#p=' + str(i) + '&tab=NewReleases'

    #SPORTS_AND_RACING - SPORTS_SIM
    PAGE_SPORTS_SIM = 'https://store.steampowered.com/category/sports_sim/#p=' + str(i) + '&tab=NewReleases'

    #SPORTS_AND_RACING - RACING_SIM
    PAGE_RACING_SIM = 'https://store.steampowered.com/category/racing_sim/#p=' + str(i) + '&tab=NewReleases'

    #THEMES

    #THEMES - ANIME
    PAGE_ANIME = 'https://store.steampowered.com/category/anime/#p=' + str(i) + '&tab=NewReleases'

    #THEMES - SPACE
    PAGE_SPACE = 'https://store.steampowered.com/category/space/#p=' + str(i) + '&tab=NewReleases'

    #THEMES - SCIENCE_FICTION
    PAGE_SCIENCE_FICTION = 'https://store.steampowered.com/category/science_fiction/#p=' + str(i) + '&tab=NewReleases'

    #THEMES - MISTERY_DETECTIVE
    PAGE_MISTERY_DETECTIVE = 'https://store.steampowered.com/category/mystery_detective/#p=' + str(i) + '&tab=NewReleases'

    #THEMES - EXPLORATION_OPEN_WORLD
    PAGE_EXPLORATION_OPEN_WORLD = 'https://store.steampowered.com/category/exploration_open_world/#p=' + str(i) + '&tab=NewReleases'

    #THEMES - SURVIVAL
    PAGE_SURVIVAL = 'https://store.steampowered.com/category/survival/#p=' + str(i) + '&tab=NewReleases'

    #THEMES - HORROR
    PAGE_HORROR = 'https://store.steampowered.com/category/horror/#p=' + str(i) + '&tab=NewReleases'

    #PLAYERS

    #PLAYERS - MULTIPLAYER_ONLINE_COMPETITIVE
    PAGE_MULTIPLAYER_ONLINE_COMPETITIVE = 'https://store.steampowered.com/category/multiplayer_online_competitive/#p=' + str(i) + '&tab=NewReleases'

    #PLAYERS - COOPERATIVE
    PAGE_COOPERATIVE = 'https://store.steampowered.com/category/multiplayer_coop/#p=' + str(i) + '&tab=NewReleases'

    #PLAYERS - MULTIPLAYER_MMO
    PAGE_MULTIPLAYER_MMO = 'https://store.steampowered.com/category/multiplayer_mmo/#p=' + str(i) + '&tab=NewReleases'

    #PLAYERS - MULTIPLAYERS
    PAGE_MULTIPLAYER = 'https://store.steampowered.com/category/multiplayer/#p=' + str(i) + '&tab=NewReleases'

    #PLAYERS - MULTIPLAYER_LOCAL_PARTY
    PAGE_MULTIPLAYER_LOCAL_PARTY = 'https://store.steampowered.com/category/multiplayer_local_party/#p=' + str(i) + '&tab=NewReleases'

    #PLAYERS - MULTIPLAYER_LAN
    PAGE_MULTIPLAYER_LAN = 'https://store.steampowered.com/category/multiplayer_lan/#p=' + str(i) + '&tab=NewReleases'

    #PLAYERS - SINGLEPLAYERS
    PAGE_SINGLEPLAYER =  'https://store.steampowered.com/category/singleplayer/#p=' + str(i) + '&tab=NewReleases'

    LIST_IMG = res.find_all('div', attrs={'class': 'col search_capsule'})
    LIST_NOME = res.find_all('div', attrs={'class': 'col search_name ellipsis'})
    LIST_EXCLUSIVIDADE = res.find_all('div', attrs={'class': 'responsive_search_name_combined'})
    LIST_SISTEMA_OPERACIONAL = res.find_all('div', attrs={'class': 'responsive_search_name_combined'})
    LIST_TIPO_DE_JOGO = res.find_all('div', attrs={'class': 'tab_item_top_tags'})
    LIST_DATA_DE_ATUALIZACAO = res.find_all('div', attrs={'class': 'col search_released responsive_secondrow'})
    LIST_PRECO_DO_JOGO = res.find_all('div', attrs={'class': 'col search_price  responsive_secondrow'})
    LIST_AVALIACOES = res.find_all('div', attrs={'class': 'col search_reviewscore responsive_secondrow'})

    LIST_IMG_TEXT = LIST_IMG.TEXT.STRIP()
    LIST_NOME_TEXT = LIST_NOME.TEXT.STRIP()
    LIST_EXCLUSIVIDADE_TEXT = LIST_EXCLUSIVIDADE.TEXT.STRIP()
    LIST_SISTEMA_OPERACIONAL_TEXT  = LIST_SISTEMA_OPERACIONAL.TEXT.STRIP()
    LIST_TIPO_DE_JOGO_TEXT  = LIST_TIPO_DE_JOGO.text.STRIP()
    LIST_DATA_DE_ATUALIZACAO_TEXT  = LIST_DATA_DE_ATUALIZACAO.TEXT.STRIP()
    LIST_PRECO_DO_JOGO_TEXT = LIST_PRECO_DO_JOGO.TEXT.STRIP()
    LIST_AVALIACOES_TEXT = LIST_AVALIACOES.TEXT.STRIP()

    print(LIST_IMG.PRETTIFY())
    print(LIST_NOME.PRETTIFY())
    print(LIST_EXCLUSIVIDADE.PRETTIFY())
    print(LIST_SISTEMA_OPERACIONAL.PRETTIFY())
    print(LIST_TIPO_DE_JOGO.PRETTIFY())
    print(LIST_DATA_DE_ATUALIZACAO.PRETTIFY())
    print(LIST_PRECO_DO_JOGO_TEXT.PRETTIFY())
    print(LIST_AVALIACOES.PRETTIFY())

    #Delete Previous CSV
    os.remove('STEAM-GAMES.csv')#remove arquivo

    #CSV Change
    with open('STEAM-GAMES.csv', 'w', newline='', encoding='utf-8') as csv_file:
    
        writer = csv.writer(csv_file)
    
        fieldnames = writer.writerow(["IMAGEM", "NOME", "EXCLUSVIDDADE", "SISTEMA_OPERACIONAL", "DATA_DE_ATUALIZACAO", "TIPO_DE_JOGO", "AVALIACOES"])
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        #writer.writerow(['htmls'])

    #Directory and Csv Control Commands
    #os.remove('nome do arquivo') #remove arquivo
    #os.removedirs('caminho do diretorio') #remove estrutura de diretorios
    #os.rename('nome antigo','nome novo') #renomeia arquivo   
    diretorio_atual = os.getcwd() #retorna diretorio atual
    #os.rmdir('nome do diretorio') #remove diretorio
    #os.mkdir('nome do diretorio') #cria diretorio
    #os.chdir('nome do diretorio') #muda diretorio (cd)
    #lista_diretorios = os.listdir('') #lista diretorio
    #os.walk('diretorio base') #navega pela arvore de diretorios    
    existe = os.path.exists('steam-games.csv') #verifica se arquivo existe
    nome_csv = os.path.basename('steam-games.csv') #retorna o nome do arquivo, sem diretorio
    caminho_csv = os.path.dirname('steam-games.csv') #retorna o caminho do arquivo, sem o nome
    data_ultimo_acesso = os.path.getatime('steam-games.csv') #retorna a data do ultimo acesso
    data_de_criacao = os.path.getctime('steam-games.csv') #retorna a data de criacao
    data_de_ultima_alteracao = os.path.getmtime('steam-games.csv') #retorna a data da ultima alteracao
    tamanho = os.path.getsize('steam-games.csv') #retorna o tamanho do arquivo em bytes
    diretorio_csv = os.path.isdir('steam-games.csv') #verifica se o arquivo passado é um diretório
    csv_diretorio = os.path.isfile('steam-games.csv') #verifica se o arquivo passado é um arquivo
    #csv_concatena_caminho = os.path.join('dir1', 'dir2') #concatena dois elementos para formar um caminho
    #csv_retorno_lista_caminho = os.path.split('dir1', 'dir2') #retorna lista com todas as partes de um caminho
    #shutil.copy('origem','destino') #copia arquivo
    #shutil.move('origem','destino') #move arquivo

    #CSV Informations
    print(diretorio_atual)
    print(existe)
    print(nome_csv)
    print(caminho_csv)
    print(data_ultimo_acesso)
    print(data_de_criacao)
    print(data_de_ultima_alteracao)
    print(tamanho)
    print(diretorio_csv)
    print(csv_diretorio)

    #CSV Viewer
    with open('STEAM-GAMES.csv', encoding='utf-8') as csv_file:

        csv_reader = csv.DictReader(csv_file, fieldnames=["IMAGEM", "NOME", "EXCLUSVIDDADE", "SISTEMA_OPERACIONAL", "DATA_DE_ATUALIZACAO", "TIPO_DE_JOGO", "AVALIACOES"])

        csv_reader.__next__()
        
        for row in csv_reader:
            print(row["IMAGEM"] + ', ' + row["NOME"] + ', ' + row["EXCLUSIVIDADE"] + ', ' + row["SISTEMA_OPERACIONAL"] + 
                ', ' + row["DATA_DE_ATUALIZACAO"] + ', ' + row["TIPO_DE_JOGO"] + ', ' + row["AVALIACOES"])

# Algorithm By Steam-games Enterprise! All Rights Reserved! Open Source 4ever. Like and Follow Project
# in GitLab - https://gitlab.com/BDAg/steam-games