from googlesearch import search
from bs4 import BeautifulSoup
import requests
import pymongo
import csv
import re
import os

# Algorithm By Seeker Enterprise! All Rights Reserved! Open Source 4ever. Like and Follow Project
# in GitLab - https://gitlab.com/BDAg/steam-games

URL = 'https://store.steampowered.com/search/?sort_by=Released_DESC&maxprice=free&tags=701'
page = requests.get(URL)

soup = BeautifulSoup(page.content, 'html.parser')

results = soup.find(id='search_resultsRows')

print(results.prettify())
