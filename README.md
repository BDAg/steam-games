# Steam-Games
![Steam GAMES](https://img.ibxk.com.br/2016/05/09/09122450744195.jpg?w=1120&h=420&mode=crop&scale=both)

## Sobre o Projeto

**Missão: Estamos sempre criando. Quando você está cercado por pessoas talentosas com a liberdade de criar, sem medo do fracasso, coisas incríveis acontecem. Vemos isso todos os dias na VALVE. De fato, alguns das nossas melhores ideias vem de nossos maiores erros. E nós estamos bem com isso! Desde 1996, esta abordagem produziu jogos premiados, tecnologias de ponta, e uma plataforma de entretenimento social inovadora. Estamos sempre à procura de obras criativas e com alto risco que podem manter o tabu vivo.**

**Visão: Ser uma empresa inovadora, dando autonomia ao nossos funcionários para produzirem novas tecnologias para o desenvolvimento, distribuição e tecnologias na forma que o usuário irá se entreter nos jogos.**

**Valores:**

**- Não há medo de falhar**

**- Responder apenas para si e para seus clientes**

**- Visão de estratégia clara**

**- Contratar pessoas certas e confiar no seu trabalho plenamente**

- Todos podem participar de qualquer projeto da empresa, qualquer um pode questionar o que o colega de trabalho está produzindo.  Não existem fatores externos a pressionando para atingir resultados, pois ela foi fundada com dinheiro próprio. Respondem apenas para si e para seus clientes. Não há um intermediário na relação da VALVE e o comprador de seus jogos. 

- A VALVE contrata expertise para desenvolvimento de uma área específica, por exemplo, o caso em que contratou o Michael Abrash. Ser uma empresa flat sem hierarquia direta. Qualquer funcionário questiona o trabalho de outro independente da função que ela exerça. 

- A VALVE é muito atenta ao que o usuário quer, tanto que todos os grandes jogos da VALVE foram baseadas nos modos de jogos criados pelos usuários. A grande aquisição que não tem nada haver com ao produto final que a VALVE sempre desenvolveu foi o DOTA 2. Um game completamente diferente, mas que foi desenvolvido por um modo de jogo de um usuário comum, exemplo também de COUNTER-STRIKE, LEFT 4 DEAD, TEAM FORTRESS e PORTAL foram jogos que tiveram uma parcela de criação dos seus usuários com as modificações dos modos de jogo.

## Arquivos do Projeto

- Para acessar o Cronograma [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/CRONOGRAMA.docx)

- Para acessar os Integrantes + Tema [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/INTEGRANTES_+_TEMA.docx)

- Para acessar o MVP  [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/MVP/MVP.pptx)

- Para acessar o Mapa de Conhecimento [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/STEAM-GAMES.xmind)

- Para acessar a Matriz de Habilidade Inicial [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/MATRIZ_DE_HABILIDADES_INICIAL.xlsx)

- Para acessar a Matriz de Habilidade Final [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/MATRIZ_DE_HABILIDADES_FINAL.xlsx)

- Para acessar as Lições Aprendidas - Marcelo [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/Licoes_-_Marcelo.docx)

- Para acessar as Lições Aprendidas - Gabriela [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/Licoes_-_Marcelo.docx)

- Para acessar o Estudo sobre Grafos  - Marcelo [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/Grafos/Grafos_-_Marcelo.docx)

- Para acessar o Estudo sobre Grafos - Gabriela [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/Grafos/Grafos_-_Gabriela.docx)

- Para acessar o Modelo de Dados [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/MODELO-DE-DADOS.xmind)

- Para acessar o Project Charter [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/PROJECT_CHARTER.pptx)

- Para acessar a Apresentação [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/STEAM-GAMES.pptx)

- Para acessar o Roteiro de Mineração de dados [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/Steam-games.txt)

## Pastas do Projeto

- Para acessar os Currículos [clique aqui.](https://gitlab.com/BDAg/steam-games/-/tree/master/Curr%C3%ADculos)

- Para acessar os Estudos [clique aqui.](https://gitlab.com/BDAg/steam-games/-/tree/master/Estudos)

- Para acessar os Grafos  [clique aqui.](https://gitlab.com/BDAg/steam-games/-/tree/master/Grafos)

- Para acessar o MVP [clique aqui.](https://gitlab.com/BDAg/steam-games/-/tree/master/MVP)

- Para acessar os Scripts [clique aqui.](https://gitlab.com/BDAg/steam-games/-/tree/master/Python%20Scripts)


## Programas Utilizados
- Para acessar o XMind  [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/XMind-2020-for-Windows-64bit-10.3.1-202101070032.exe)

- Para acessar o NoSqlBoosterMongo [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/nosqlbooster4mongo-5.2.10.exe)

- Para acessar o Mapa de LightShot [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/setup-lightshot.exe)

- Para acessar a DezignForDatabases [clique aqui.](https://gitlab.com/BDAg/steam-games/-/blob/master/setup_dezign.exe)

## Andamento do Projeto

- [X] Entrega 01
- [X] Entrega 02
- [X] Entrega 03
- [X] Entrega 04
- [ ] Sprint 1
- [ ] Sprint 2
- [X] Sprint 3
- [X] Fechamento do Projeto

## Time

- Marcelo Augusto [[Perfil.]](https://gitlab.com/marcelinhoshow1)
- Gabriela Pedroso [[Perfil.]](https://gitlab.com/Gabys)

## Professor

- Mauricio Duarte [[Perfil.]](https://gitlab.com/BDAg/steam-games)
- João Ricardo [[Perfil.]](https://gitlab.com/BDAg/steam-games)
